import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { FC } from "react";
import Image from "next/image";
import { notFound } from "next/navigation";

interface pageProps {
  params: {
    slug: string;
  };
}

const page: FC<pageProps> = ({ params }) => {
  const { slug } = params;

  if (slug !== "login" && slug !== "register") return notFound();

  const isLogin = params.slug === "login";

  return (
    <div className="container">
      <div className="flex text-white justify-center p-8">
        <div className="hidden md:block relative w-1/3">
          <Image
            src={isLogin ? "/auth/back-login.png" : "/auth/back-register.png"}
            alt={"Background"}
            width={"600"}
            height={"700"}
            className="h-full"
            objectFit="cover"
            objectPosition="center"
            style={{ filter: "brightness(0.7)" }}
          />
          <div className="absolute w-3/4 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col gap-3 p-1">
            <p className="text-2xl font-bold">
              {isLogin
                ? "Content de vous revoir parmi nous !"
                : "Première visite ? Rejoignez-nous"}
            </p>
            <p className=" text-xs">
              {isLogin
                ? "Vous possédez déjà un compte, connectez-vous à l’aide de vos identifiants"
                : "Vous ne possédez pas de compte, commençait dès maintenant à en créer un et partager vos connaissances."}
            </p>
          </div>
        </div>
        <div className="flex flex-col gap-8 bg-primary p-8 w-1/3">
          <div>
            <p className="font-semibold text-4xl mb-5">
              {isLogin ? "Login" : "Enregistrez-vous"}
            </p>
            <p className="mb-5">
              {isLogin
                ? "Connectez-vous pour accéder aux formations d’e-learning."
                : "Créer un compte pour accéder aux formations d’e-learning."}
            </p>
          </div>
          <form
            action={isLogin ? "/login" : "/register"}
            className="flex flex-col gap-8"
          >
            {isLogin ? null : (
              <Input
                type="text"
                placeholder="Nom d'utilisateur"
                className="text-black"
              />
            )}
            <Input
              type="email"
              placeholder="Adresse email"
              className="text-black"
            />
            <Input
              type="password"
              placeholder="Mots de passe"
              className="text-black"
            />
          </form>

          <Button
            className="w-full text-black"
            variant="secondary"
            type="submit"
          >
            Connexion
          </Button>
        </div>
      </div>
    </div>
  );
};

export default page;
