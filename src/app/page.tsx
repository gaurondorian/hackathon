import CardLanding from "@/components/CardLanding";
import { Button } from "@/components/ui/button";
import { Card } from "@/types/card";
import { ChevronRight } from "lucide-react";
import Image from "next/image";
import Link from "next/link";

export default function Home() {
  const cards: Array<Card> = [
    {
      title: "Sujet",
      description:
        "Trouver un sujet qui vous passione et que vous aimeriez transmettre à des fins pédagogique.",
    },
    {
      title: "Création",
      description:
        "Grâce à notre interface d’édition en temps réel vous pouvez, créer un pdf aux grêt de vos envies.",
    },
    {
      title: "Publication",
      description:
        "Une fois que vous êtes satisfais de vôtre cours il ne reste plus cas qu’a le publié en pressant un simple boutton.",
    },
    {
      title: "Edition",
      description:
        "loremp ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
      title: "Partage",
      description:
        "loremp ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
      title: "Apprentissage",
      description:
        "loremp ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
  ];

  return (
    <main className="flex flex-col items-center justify-between">
      <div className="flex gap-5 bg-primary p-24 text-white">
        <div className="flex flex-col gap-8">
          <p className="text-5xl font-bold">
            Commencez à créer vos cours maintenant !
          </p>
          <p>
            IDE - Le@rn!ing première plateforme de création de cours intéractif.
            Rejoignez l’aventure, maintenant et participez à la création des
            cours de demain.
          </p>

          <div>
            <Link href="/auth/register">
              <Button variant="secondary">Enregistrez-vous</Button>
            </Link>

            <Link href="/auth/login">
              <Button variant="link" className="text-white">
                Voir les cours <ChevronRight className="w-5 h-5" />
              </Button>
            </Link>
          </div>
        </div>

        <div className="flex">
          <Image src="/landing.svg" alt={""} width={600} height={400} />
        </div>
      </div>

      <div className="flex justify-around h-40 bg-[#EEF4FA] w-full">
        <div className="flex items-center gap-8">
          <div>
            <p className="text-3xl font-bold">21.000+</p>
            <span className="opacity-60">Utilisateurs</span>
          </div>
          <div>
            <p className="text-3xl font-bold">100 +</p>
            <span className="opacity-60">Cours</span>
          </div>
          <div>
            <p className="text-3xl font-bold">150 +</p>
            <span className="opacity-60">Templates</span>
          </div>
        </div>
        <div className="flex items-center gap-6">
          <p className="text-xl font-bold">Sup de vinci</p>
          <p className="text-xl font-bold">Digital school</p>
          <p className="text-xl font-bold">Business school</p>
        </div>
      </div>

      <div>
        <p className="font-bold text-5xl text-center p-8">
          Comment créer vos cours sur <br />
          [IDE - Learn!ing]
        </p>
        <div className="flex items-center justify-center">
          <div className="grid grid-cols-3 grid-rows-2 content-center">
            {cards.map((card, index) => (
              <CardLanding {...card} index={index} key={index} />
            ))}
          </div>
        </div>
      </div>
    </main>
  );
}
