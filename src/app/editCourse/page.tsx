"use client";
//index.tsx
import { OutputData } from "@editorjs/editorjs";
import type { NextPage } from "next";
import dynamic from "next/dynamic";
import React, { useState } from "react";
import SideBar from "@/components/SideBar";
import EditorJsRenderer from "@/components/EditorJsRenderer";
import Image from "next/image";
import Eye from "../../../public/eye.svg";
import { Button } from "@/components/ui/button";
import Schools from "../../../public/jsonFiles/school.json";

// important that we use dynamic loading here
// editorjs should only be rendered on the client side.
const EditorBlock = dynamic(
  () => import("../../components/EditorJsComponent"),
  {
    ssr: false,
  }
);

const Home: NextPage = () => {
  //state to hold output data. we'll use this for rendering later
  const [data, setData] = useState<OutputData>();
  const [renderPreview, setRenderPreview] = useState(true);
  const [curSchool, setCurSchool] = useState(0);
  function handleChange() {
    setRenderPreview(!renderPreview);
  }

  // @ts-ignore
  function handleChangeSchool(e) {
    setCurSchool(e.target.id);
    console.log(curSchool);
  }
  return (
    <div className="flex bg-[#F4F6FC]">
      <SideBar />
      <div className="flex flex-col w-full h-full justify-self-center px-16">
        <div className=" my-8">
          <h3>Matière - Nom du formateur</h3>
          <div className="flex flex-row place-items-center">
            <h1 className="text-4xl text-medium">Nom du cours</h1>{" "}
            <Image
              onClick={handleChange}
              className="w-8 ml-4 mt-1 hover:cursor-pointer"
              src={Eye}
              alt="view preview"
            />{" "}
            <p className="mt-1 ml-2 text-xs text-gray-400">(preview)</p>
          </div>
        </div>
        <div className="flex flex-row h-full w-full justify-center align-middle gap-16 place-self-center">
          <div className="bg-white w-full rounded-2xl p-4 container mb-8">
            <EditorBlock
              data={data}
              onChange={setData}
              holder="editorjs-container"
            />
          </div>
          {renderPreview ? (
            <div className="flex flex-col h-full w-full justify-center align-middle">
              <h1 className="p-4">Preview</h1>
              <div className="bg-white  rounded-2xl p-4 w-full flex flex-col">
                <h1 className="text-lg font-bold text-gray-400">
                  {Schools[curSchool].name}
                </h1>
                <div className="p-8">
                  {data && <EditorJsRenderer data={data} />}
                </div>
                <Image
                  id="0"
                  onClick={handleChangeSchool}
                  className="hover:cursor-pointer place-self-center justify-self-center opacity-60"
                  width="32"
                  height="32"
                  src={Schools[curSchool].logo}
                  alt="logo esna"
                />
              </div>
              <div className=" my-8 ml-[40%]">
                <h1 className="mb-4">Vos écoles :</h1>
                <div className="flex flex-row gap-4 ">
                  <Image
                    id="0"
                    onClick={handleChangeSchool}
                    className="hover:cursor-pointer"
                    width="32"
                    height="32"
                    src={Schools[0].logo}
                    alt="logo esna"
                  />
                  <Image
                    id="1"
                    onClick={handleChangeSchool}
                    className="hover:cursor-pointer"
                    width="32"
                    height="32"
                    src={Schools[1].logo}
                    alt="logo epitech"
                  />
                  <Image
                    id="2"
                    onClick={handleChangeSchool}
                    className="hover:cursor-pointer"
                    width="32"
                    height="32"
                    src={Schools[2].logo}
                    alt="logo epsi"
                  />
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Home;
