import { Card } from "@/types/card";
import { FC } from "react";

type CardLandingProps = Card & {
  index: number;
};

const CardLanding: FC<CardLandingProps> = ({ description, title, index }) => {
  return (
    <div className="bg-[#F4F6FC] p-5 flex flex-col gap-4 rounded-md w-4/5">
      <div className="bg-[#2405F2] w-8 h-8 p-2 flex items-center justify-center">
        <p className="text-white">{index}</p>
      </div>
      <p className="font-bold text-2xl">{title}</p>
      <p className="opacity-60">{description}</p>
    </div>
  );
};

export default CardLanding;
