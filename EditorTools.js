import Code from "@editorjs/code";
import Header from "@editorjs/header";
import Paragraph from "@editorjs/paragraph";
import Table from '@editorjs/table';
import NestedList from '@editorjs/nested-list';
export const EDITOR_TOOLS = {
    header: Header,
    paragraph: Paragraph,
    code: Code,
    list: {
        class: NestedList,
        inlineToolbar: true,
        config: {
            defaultStyle: 'unordered'
        },
    },
};