/** @type {import('next').NextConfig} */
const nextConfig = {
    images : {
        domains : ["www.formation-industrie.bzh","epitech.bj","studyadv.s3.amazonaws.com"]
    }
}

module.exports = nextConfig
