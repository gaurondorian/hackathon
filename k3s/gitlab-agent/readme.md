# What is a Gitlab agent

It's an agent who is in your kubernetes cluster that allow you to do kubectl command inside your cluster

# Deploy Gitlab agent

**Requirments: Kubernetes (k3s,microk8s,minikube) need to be installed on your server**  

To deploy Gitlab agent, follow  these steps:  

First step you need to add this path in your repository.

.gitlab/agents/<name_of_your_Gitlab_agent>/config.yaml (config.yaml can be empty)  
  
Second step in gitlab: click on the section "Operate" and go to "Kubernetes Cluster" and click on "Connect a cluster".

# How to use it in a gitlabci  

You need to add a context line in your ci module:  

```yaml
# Exécution des tests Kubernetes
kube_test:
  stage: kube_test
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config use-context NaThAnBl/hackathon:kube-agent
    - kubectl get pods -n gitlab
  when: manual
```
