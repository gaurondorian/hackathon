# Deploy Gitlab runner

To deploy Gitlab runner, follow  these steps:  

**Requirments: Helm need to be installed** 

- Add Gitlab Helm repo:
```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
```

- Add your token at the line 13 of `values.yaml`
- Deploy the runner:
```bash
helm install --namespace gitlab gitlab-runner -f values.yaml gitlab/gitlab-runner
```