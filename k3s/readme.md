# Installation of k3s ⛵

K3s is a lightweight, easy-to-install Kubernetes distribution designed for resource-constrained environments such as edge devices, IoT devices, and small production deployments.  
It includes all the core components of Kubernetes but with a smaller footprint and fewer dependencies, making it easier to run on low-power devices or in environments with limited network bandwidth.  
K3s can be deployed quickly and easily using a single binary, and it's designed to be highly available and resilient, making it a popular choice for edge computing use cases.

## 1. Connect with an account (need sudo privileges) :heavy_plus_sign:

Connect to the SSH server where teh cluster is instaled. (Master)

```bash
ssh <username>@<server>
```

## 2. Install k3s installation script and execute it :rocket:

```bash
curl -sfL https://get.k3s.io | sh -
```

## 3. Add kube command to an account (need sudo privileges) ⚙️

Reproduce these steps to get Kubernetes config file on your account:  

- Go to your home directory:  
```bash
cd ~/
```
- Copy paste these lines: 

```bash
mkdir .kube
touch .kube/config
sudo cp /etc/rancher/k3s/k3s.yaml .kube/config
sudo chmod go-r ~/.kube/config
echo 'export KUBECONFIG=~/.kube/config' >> ~/.bashrc
source ~/.bashrc
```