import getConfig from 'next/config';

const { serverRuntimeConfig } = getConfig();

export const project_root = serverRuntimeConfig.PROJECT_ROOT;